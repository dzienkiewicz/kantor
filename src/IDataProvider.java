import java.io.IOException;

public interface IDataProvider {
    Repository loadData() throws IOException;
}
