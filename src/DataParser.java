import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import java.util.LinkedList;
import java.util.List;

public abstract class DataParser {
    public static Repository createRepository(Document xml){
        NodeList items= xml.getElementsByTagName("pozycja");
        List<Currency> currency= new LinkedList<>();

        for(int i=0; i< items.getLength(); i++){
            String temporary = xml.getElementsByTagName("kurs_sredni").item(i).getTextContent();
            Currency curr = new Currency(xml.getElementsByTagName("nazwa_waluty").item(i).getTextContent(),
                    xml.getElementsByTagName("kod_waluty").item(i).getTextContent(),
                    Double.parseDouble(temporary.replace(',', '.')),
                    Integer.parseInt(xml.getElementsByTagName("przelicznik").item(i).getTextContent()));
        currency.add(curr);
        }
        return new Repository(currency);
    }
}
