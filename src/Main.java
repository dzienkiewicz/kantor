import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        try {
            NDataProvider nDataProvider = new NDataProvider();
            Repository repository = nDataProvider.loadData();
            UserData userData=new UserData(repository);
            userData.consoleInput();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
