public class Currency {

    private String name;
    private String code;
    private double value;
    private int factor;
    Currency(Currency currency){
        this.name=currency.getName();
        this.code=currency.getCode();
        this.value=currency.getValue();
        this.factor=currency.getFactor();
    }
    Currency(String name, String code, double value, int factor){
        this.name=name;
        this.code=code;
        this.value=value;
        this.factor=factor;
    }
    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public void setFactor(int factor) {
        this.factor = factor;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public double getValue() {
        return value;
    }

    public int getFactor() {
        return factor;
    }

}
