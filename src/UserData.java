import java.util.Scanner;

public class UserData {
    Repository repo=new Repository();
    Scanner obj=new Scanner(System.in);

    UserData(Repository repo){
        this.repo=repo;
    }
    public boolean containCurrency(String code){
        for(Currency curr:repo.getCurrency()){
            if(code.equals(curr.getCode())) return true;
        }
        return false;
    }

    public void consoleInput(){
        while (true){
        Menu.printMenu();
        int menu=obj.nextInt();
        if(menu<1||menu>4){
            System.out.println("You choose wrong number!");
            consoleInput();
        }
        switch (menu){
            case 1:
                Menu.printAllCurrency(repo);
                break;
            case 2:
                obj.nextLine();
                boolean find=false;
                System.out.println("Write currency code: ");
                String tmp=obj.nextLine();
                for(Currency curr:repo.getCurrency()){
                    if(tmp.equals(curr.getCode())){
                        Menu.printCurr(curr);
                        find=true;
                        break;
                    }}
                if(!find)System.out.println("We dont support entered currency");
                break;
            case 3:
                obj.nextLine();
                System.out.println("Choose currency that you pay:");
                String line1=obj.nextLine();
                if (!containCurrency(line1)){
                    System.out.println("We dont support entered currency");
                    break;
                }
                System.out.println("Choose currency that you want to buy");
                String line2=obj.nextLine();
                if (!containCurrency(line2)){
                    System.out.println("We dont support entered currency");
                    break;
                }
                System.out.println("How much "+line2+" you want yo buy?");
                double ammount=obj.nextDouble();
                double result=Calculate.calculate(repo,line1,line2,ammount);

                System.out.println("You can buy "+ammount+" "+line2+" for "+result+" "+line1);
                break;
            case 4:
                return;
        }
    }}
}
