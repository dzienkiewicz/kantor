import java.util.Scanner;

public class Menu {
    static void printAllCurrency(Repository repo){
        int iterator=0;
        for (Currency curr:repo.getCurrency()) {
            iterator++;
            System.out.println(iterator+". "+curr.getName()+" code: "+curr.getCode());
        }
    }
    static void printCurrency(Currency curr){
                printCurr(curr);
        }

    static void printMenu(){
        System.out.println("--------MENU--------");
        System.out.println("1. Show All Currency");
        System.out.println("2. Show one Currency");
        System.out.println("3. Exhange money");
        System.out.println("4. Exit");
    }
    static void printCurr(Currency curr){
        System.out.println(curr.getName());
        System.out.println(curr.getCode());
        System.out.println(curr.getFactor());
        System.out.println(curr.getValue());
    }
}
