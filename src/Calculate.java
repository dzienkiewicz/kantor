public abstract class Calculate {
    public static double calculate(Repository repo, String curr1, String curr2, double ammount){
        Currency currency1=null, currency2 = null;
        for (Currency curr:repo.getCurrency()){
            if(curr.getCode().equals(curr1)) currency1=new Currency(curr);
            else if (curr.getCode().equals(curr2)) currency2=new Currency(curr);
        }
        if (currency1==null||currency2==null) throw new IllegalArgumentException("We dont have that currency");

        return (ammount*currency2.getValue()*currency2.getFactor()/(currency1.getFactor()*currency1.getValue()));
    }
}
