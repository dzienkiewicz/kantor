import java.io.*;
import java.net.URL;

public abstract class Connection {
    public static byte[] connect(String urlStr) throws IOException {
        URL Url= new URL(urlStr);
        InputStream in= new BufferedInputStream(Url.openStream());
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        byte[] buffer = new byte[2048];
        int count=0;
        while((count = in.read(buffer)) > 0)
        {
            baos.write(buffer, 0, count);
        }
        baos.close();
        in.close();
        return baos.toByteArray();
    }

}
