import org.w3c.dom.Document;

import java.io.IOException;

public class NDataProvider implements IDataProvider{
    @Override
    public Repository loadData() throws IOException{
        byte[] result= Connection.connect("http://www.nbp.pl/kursy/xml/LastA.xml");
        Document xml = ParserXML.parse(result);
        return DataParser.createRepository(xml);
    }
}
