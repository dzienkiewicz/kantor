import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Repository {
    private List<Currency> currency=new LinkedList<Currency>();
    public void addCurrency(Currency currency) {
        this.currency.add(currency);
    }

    Repository(List<Currency> dane){
        this.currency=dane;
        this.currency.add(new Currency("Polski złoty","PLN",1,1));
    }
    Repository(){

    }

    public List<Currency> getCurrency() {
        return currency;
    }
}
